class Administrator {
    constructor(adminName) {
        this.adminName = adminName;
        this._email = "";
    }

    set email(str) {
        if (!/\S+@\S+\.\S+/.test(str) || !str.trim()) {
            throw new Error("Формат e-mail адреса должен быть example@xxx.xx");
        }
        this._email = str;
    }

    get email() {
        return this._email;
    }

    updateCatalog() { }
}

const admin = new Administrator("Ilya");

try {
    admin.email = "i.razhkouski@gmail.com";
} catch (e) {
    console.log(e.message);
}

console.log(admin);

class User {
    constructor(userId, loginStatus) {
        this.userId = userId;
        this._password = "";
        this.loginStatus = loginStatus;
        this._registerDate = "";
    }

    set registerDate(str) {
        if (!Date.parse(str) || !str.trim()) {
            throw new Error("Формат даты должен быть mm/dd/yyyy.");
        }
        this._registerDate = str;
    }

    get registerDate() {
        return this._registerDate;
    }

    set password(str) {
        if (!str.trim() || str.length < 8 || !/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str)) {
            throw new Error("Пароль должен состоять из 8+ символов, а также содержать один спецсимвол.");
        }
        this._password = str;
    }

    get password() {
        return this._password;
    }

    verifyLogin() { }
}

const user = new User(1, "razhkouski");

try {
    user.password = "*us8r5Pw";
} catch (e) {
    console.log(e.message);
}

try {
    user.registerDate = "06/30/2021";
} catch (e) {
    console.log(e.message);
}

console.log(user);

class Customer {
    constructor(address, shippingInfo) {
        this._customerName = "";
        this.address = address;
        this._email = "";
        this._creditCardInfo = "";
        this.shippingInfo = shippingInfo;
        this._accountBalance = "";
    }

    set customerName(str) {
        if (!isNaN(str) || !str.trim() || str.length < 4) {
            throw new Error("Имя пользователя не должно состоять только из чисел и иметь длину менее 4 символов.");
        }
        this._customerName = str;
    }

    get customerName() {
        return this._customerName;
    }

    set email(str) {
        if (!/\S+@\S+\.\S+/.test(str) || !str.trim()) {
            throw new Error("Формат e-mail адреса должен быть example@xxx.xx");
        }
        this._email = str;
    }

    get email() {
        return this._email;
    }

    set creditCardInfo(value) {
        if (!isNaN(value) || value.replace(/\s+/g, '').length !== 16 || !value.trim()) {
            throw new Error("Длина номера кредитной карты должна составлять 16 символов.");
        }
        this._creditCardInfo = value;
    }

    get creditCardInfo() {
        return this._creditCardInfo;
    }

    set accountBalance(value) {
        if (isNaN(value) || !value.trim()) {
            throw new Error("Допустимо только числовое значение.");
        }
        this._accountBalance = value;
    }

    get accountBalance() {
        return this._accountBalance;
    }

    register() { }
    login() { }
    updateProfile() { }
}

const customer = new Customer(
    "Minsk, Mayakovsky street",
    "test"
);

try {
    customer.customerName = "randomUser";
} catch (e) {
    console.log(e.message);
}

try {
    customer.email = "random.user@gmail.com";
} catch (e) {
    console.log(e.message);
}

try {
    customer.creditCardInfo = "1111 2222 3333 4444";
} catch (e) {
    console.log(e.message);
}

try {
    customer.accountBalance = "123";
} catch (e) {
    console.log(e.message);
}

console.log(customer);

class ShoppingCart {
    constructor(cartId, dateAdded) {
        this.cartId = cartId;
        this._productId = "";
        this._quantity = "";
        this.dateAdded = dateAdded;
    }

    set productId(value) {
        if (isNaN(value) || value < 1) {
            throw new Error("Допустимо только числовое значение больше 0.");
        }
        this._productId = value;
    }

    get productId() {
        return this._productId;
    }

    set quantity(value) {
        if (isNaN(value) || value < 1) {
            throw new Error("Допустимо только числовое значение больше 0.");
        }
        this._quantity = value;
    }

    get productId() {
        return this._quantity;
    }

    addCartItem() { }
    updateQuantity() { }
    viewCartDetails() { }
    checkOut() { }
}

const cart = new ShoppingCart(1, "06.30.2021");

try {
    cart.productId = 10392;
} catch (e) {
    console.log(e.message);
}

try {
    cart.quantity = 1;
} catch (e) {
    console.log(e.message);
}

console.log(cart);

class OrderDetails {
    constructor(orderId, productId, productName, unitCost, subTotal) {
        this.orderId = orderId;
        this.productId = productId;
        this.productName = productName;
        this._quantity = "";
        this.unitCost = unitCost;
        this.subTotal = subTotal;
    }

    set quantity(value) {
        if (isNaN(value) || value < 1) {
            throw new Error("Допустимо только числовое значение больше 0.");
        }
        this._quantity = value;
    }

    get quantity() {
        return this._quantity;
    }

    calcPrice() { }
}

const orderDetails = new OrderDetails(1, 10392, "laptop", 1699, 100);

try {
    orderDetails.quantity = 1;
} catch (e) {
    console.log(e.message);
}

console.log(orderDetails);

class ShippingInfo {
    constructor(shippingId, shippingCost) {
        this.shippingId = shippingId;
        this._shippingType = "";
        this.shippingCost = shippingCost;
        this._shippingRegionId = "";
    }

    set shippingType(str) {
        if (!str.trim() || str !== "air" || str !== "ship") {
            throw new Error("Доступно только 2 метода доставки: самолёт и корабль.");
        }
        this._shippingType = str;
    }

    get shippingType() {
        return this._shippingType;
    }

    set shippingCost(value) {
        if (isNaN(value) || value < 1) {
            throw new Error("Допустимо только числовое значение больше 0.");
        }
        this._shippingCost = value;
    }

    get shippingCost() {
        return this._shippingCost;
    }

    updateShippingInfo() { }
}
const shipping = new ShippingInfo(5, 25);

try {
    shipping.shippingType = "train";
} catch (e) {
    console.log(e.message);
}

try {
    shipping.shippingCost = 0;
} catch (e) {
    console.log(e.message);
}

console.log(shipping);

class Order {
    constructor(dateShipped, customerName, customerId, shippingId) {
        this._orderId = "";
        this._dateCreated = "";
        this.dateShipped = dateShipped;
        this.customerName = customerName;
        this.customerId = customerId;
        this._status = "";
        this.shippingId = shippingId;
    }

    set orderId(value) {
        if (isNaN(value) || value < 1) {
            throw new Error("Допустимо только числовое значение больше 0.");
        }
        this._orderId = value;
    }

    get orderId() {
        return this._orderId;
    }

    set dateCreated(str) {
        if (!Date.parse(str) || !str.trim()) {
            throw new Error("Формат даты должен быть mm/dd/yyyy.");
        }
        this._dateCreated = str;
    }

    get dateCreated() {
        return this._dateCreated;
    }

    set status(str) {
        if (!str.trim() || str !== "complete" || str !== "process") {
            throw new Error("Доступно только два статуса: выполнено и в процессе.");
        }
        this._status = str;
    }

    get status() {
        return this._status;
    }

    placeOrder() { }
}

const order = new Order("07.01.2021", "randomHuman", 1, 10);

try {
    order.orderId = 2;
} catch (e) {
    console.log(e.message);
}

try {
    order.status = "approve";
} catch (e) {
    console.log(e.message);
}

try {
    order.dateCreated = "06.30.2021"
} catch (e) {
    console.log(e.message);
}

console.log(order);